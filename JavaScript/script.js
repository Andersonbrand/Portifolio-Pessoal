const elementBtnOne = document.querySelector('#buttonOne')
const elementBtnTwo = document.querySelector('#buttonTwo')
const elementBtnTree = document.querySelector('#buttonTree')
const elementBtnFour = document.querySelector('#buttonFour')

elementBtnOne.addEventListener('click', function () {
    window.scrollTo(0, 570)
})

elementBtnTwo.addEventListener('click', function () {
    window.scrollTo(0, 1070)
})

elementBtnTree.addEventListener('click', function () {
    window.scrollTo(0, 1570)
})

elementBtnFour.addEventListener('click', function () {
    window.scrollTo(0, 4670)
})

const whatsBtn = document.querySelector('#whatsappp')
const emailBtn = document.querySelector('#e-mail')
const linkedinBtn = document.querySelector('#linkedin')
const gitBtn = document.querySelector('#github')

whatsBtn.addEventListener("click", function () {
    window.open("https://api.whatsapp.com/send/?phone=5577999106357&text&type=phone_number&app_absent=0", "_blank")
})

emailBtn.addEventListener("click", function () {
    window.open("https://outlook.live.com/mail/0/", "_blank")
})

linkedinBtn.addEventListener("click", function () {
    window.open("https://www.linkedin.com/in/anderson-tbrand%C3%A3o/", "_blank")
})

gitBtn.addEventListener("click", function () {
    window.open("https://github.com/Andersonbrand", "_blank")
})

const acessRemoteOne = document.querySelector('#link-dev-previous')
const acessRemoteTwo = document.querySelector('#link-dev-convert')
const acessRemoteTree = document.querySelector('#link-dev-menu')
const acessRemoteFour = document.querySelector('#link-dev-shopping')

acessRemoteOne.addEventListener('click', function () {
    window.open("https://strong-piroshki-28f09b.netlify.app", "_blank")
})

acessRemoteTwo.addEventListener('click', function () {
    window.open("https://devconvertmoney.netlify.app", "_blank")
})

acessRemoteTree.addEventListener('click', function () {
    window.open("https://github.com/Andersonbrand/DevClub/tree/master/Projetos/Projeto%201", "_blank")
})

acessRemoteFour.addEventListener('click', function () {
    window.open("https://div-shopping.netlify.app", "_blank")
})